# [[]](https://apk-packages-demo.gitlab.io/discount/) discount

A Markdown to HTML translator written in C. https://pkgs.alpinelinux.org/packages?name=discount

# See also
* [notes-on-computer-programming-languages/markdown
  ](https://gitlab.com/notes-on-computer-programming-languages/markdown)

# Extensions
* SVG images [![PHPPackages Rank](http://phppackages.org/p/erusev/parsedown/badge/rank.svg)](http://phppackages.org/p/erusev/parsedown)
* Adaptive screen and line warping, page width on narrow screens... This is OK.
* Table of contents
  * No idea if possible with discount

# [Edit this page](https://gitlab.com/apk-packages-demo/discount/edit/master/README.md)